#ifndef FILELOADERROREXCEPTION_H
#define FILELOADERROREXCEPTION_H
#include <stdexcept>

class FileLoadErrorException : public std::exception
{
public:
    FileLoadErrorException(const std::string &str);
    virtual const char *what() const noexcept;

private:
    std::string m_str;
};

#endif // FILELOADERROREXCEPTION_H
