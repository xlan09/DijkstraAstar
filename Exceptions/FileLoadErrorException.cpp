#include "FileLoadErrorException.h"

FileLoadErrorException::FileLoadErrorException(const std::string &str) : m_str(str)
{

}

const char *FileLoadErrorException::what() const noexcept
{
    return m_str.c_str();
}
