# Copyrights
- GNU Licence

# Dependencies
- Qt5.4.2 or higher
- pugixml 1.7 or higher, lower versions are not tested
- log4cplus 1.2.0 or higher, lower versions are not tested

# How to Use
- Easy to use, see the GUI

![Dijkstra and Astar GUI image can not show properly. Click to view](Gui.jpg)
