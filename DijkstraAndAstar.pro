#-------------------------------------------------
#
# Project created by QtCreator 2016-03-30T23:03:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DijkstraAndAstar
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    UI/UIModel.cpp \
    UI/UIView.cpp \
    Common/ScenePos.cpp \
    Common/SceneSize.cpp \
    Common/ConfigService.cpp \
    Exceptions/FileLoadErrorException.cpp \
    Common/LogUtil.cpp \
    Packages/pugixml/src/pugixml.cpp \
    Core/Graph.cpp \
    Core/Node.cpp \
    Core/SearchUtil.cpp

HEADERS  += MainWindow.h \
    UI/UIModel.h \
    UI/UIView.h \
    Common/ScenePos.h \
    Common/SceneSize.h \
    Common/ConfigService.h \
    Exceptions/FileLoadErrorException.h \
    Common/LogUtil.h \
    Packages/pugixml/src/pugixml.hpp \
    Core/Graph.h \
    Core/Node.h \
    Core/SearchUtil.h

CONFIG += C++11

INCLUDEPATH += $$PWD/Packages/pugixml/src

LIBS += -L/usr/local/lib/ -llog4cplus
