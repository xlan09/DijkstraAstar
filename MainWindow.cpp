#include "MainWindow.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QShortcut>
#include <QComboBox>
#include <QString>
#include <QPainter>
#include <QStatusBar>
#include <QPixmap>
#include <UI/UIModel.h>
#include "UI/UIView.h"

MainWindow::MainWindow() {
    setWindowTitle("Dijkstar and A*");

    QPushButton *newGraph = new QPushButton(this);
    newGraph->setText("New Graph");
    QPushButton *reset = new QPushButton(this);
    reset->setText("Reset");
    QPushButton *search = new QPushButton(this);
    search->setText("Search Path");

    QLabel *labelStartPosLegend = new QLabel(this);
    QLabel *labelStartPos = new QLabel("StartPos: ", this);
    QLabel *labelGoalPos = new QLabel("GoalPos: ", this);
    QLabel *labelGoalPosLegend = new QLabel(this);

    // draw startpos and goalpos legend
    int pixMapWidth = 20;
    int pixMapHeight = 20;
    QPixmap pixmap(pixMapWidth,pixMapHeight);
    pixmap.fill(QColor("transparent"));
    QPainter painter(&pixmap);
    painter.setBrush(QBrush(Qt::yellow));
    painter.drawEllipse(0,0,pixMapWidth,pixMapHeight);
    labelStartPosLegend->setPixmap(pixmap);
    painter.setBrush(QBrush(Qt::red));
    painter.drawEllipse(0,0,pixMapWidth,pixMapHeight);
    labelGoalPosLegend->setPixmap(pixmap);
    QHBoxLayout *hBoxLayout = new QHBoxLayout();
    hBoxLayout->addWidget(labelStartPos);
    hBoxLayout->addWidget(labelStartPosLegend);
    hBoxLayout->addWidget(labelGoalPos);
    hBoxLayout->addWidget(labelGoalPosLegend);

    QComboBox *comboBox = new QComboBox(this);
    comboBox->addItem("Dijkstra");
    comboBox->addItem("A*");

    UIModel *model = new UIModel(this, comboBox);
    UIView *viewer = new UIView(model, this);

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(newGraph, 0, 0);
    layout->addWidget(reset, 0, 1);
    layout->addWidget(search, 0, 2);
    layout->addWidget(comboBox, 1, 0);
    layout->addLayout(hBoxLayout,1,1,1,2);
    layout->addWidget(viewer, 2, 0, 5, 5);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);
    this->setCentralWidget(centralWidget);

    //  make the buttons do things
    connect(newGraph, SIGNAL(clicked()), model, SLOT(slot_newGraph()));
    connect(reset, SIGNAL(clicked()), model, SLOT(slot_reset()));
    connect(search, SIGNAL(clicked()), model, SLOT(slot_search()));
}

MainWindow::~MainWindow() {

}
