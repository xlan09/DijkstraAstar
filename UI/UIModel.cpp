#include "UIModel.h"
#include "Common/ConfigService.h"
#include "Core/Node.h"
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>
#include <math.h>
#include <stdlib.h> // srand
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include <QComboBox>
#include <stdexcept>
#include <Common/LogUtil.h>

UIModel::UIModel(QObject *parent, QComboBox *algorithmType) : QGraphicsScene(parent), m_algorithmType(algorithmType) {
    initUIModel();
}

void UIModel::initUIModel() {
    float width = ConfigService::instance()->initializeFromConfig("sceneSize", "width", 600);
    float height = ConfigService::instance()->initializeFromConfig("sceneSize", "height", 800);

    m_sceneSize = SceneSize(ScenePos(0, 0), width, height);
    setSceneRect(m_sceneSize.NWCorner().x(), m_sceneSize.NWCorner().y(), m_sceneSize.width(), m_sceneSize.height());

    m_graph = Graph();
    m_startPos = nullptr;
    m_startNode = nullptr;
    m_goalPos = nullptr;
    m_goalNode = nullptr;
}

void UIModel::drawGraph() {
    std::vector<std::shared_ptr<Node> > nodes = m_graph.nodes();
    for(unsigned int i = 0; i < nodes.size(); ++i) {
        drawNode(nodes[i].get());
        for(unsigned int j = 0; j < nodes[i]->neighbors().size(); ++j) {
            drawLine(nodes[i].get(), nodes[i]->neighbors()[j]);
        }
    }
}

void UIModel::drawGraphDFS() {
    if (m_graph.getNumNodes() == 0) return;
    std::unordered_set<Node *> visited;
    drawGraphDFSHelper(m_graph.getRoot(), visited);
}

void UIModel::drawGraphDFSHelper(Node *currNode, std::unordered_set<Node *> &visited) {
    if (currNode == nullptr) return;

    if (visited.find(currNode) == visited.end()) {
        visited.insert(currNode); // mark it visited
        // draw this node
        drawNode(currNode);
        std::vector<Node *> neighbors = currNode->neighbors();
        for(unsigned int i = 0; i < neighbors.size(); ++i) {
            // draw line between neighbors
            drawLine(currNode, neighbors[i]);
            drawGraphDFSHelper(neighbors[i], visited);
        }
    }
}

void UIModel::drawNode(const Node *ptr) {
    if (ptr == nullptr) return;

    float x = ptr->pos().x();
    float y = ptr->pos().y();
    QGraphicsEllipseItem *item  = addEllipse(x, y, 2, 2);
    item->setBrush(Qt::blue);
    addItem(item);

    // draw node Id
    QGraphicsTextItem *textItem = addText(QString::fromStdString(std::to_string(ptr->Id())));
    textItem->setPos(x, y);
    addItem(textItem);
}

void UIModel::drawLine(const Node *node1, const Node *node2) {
    if (node1 == nullptr || node2 == nullptr) return;

    QGraphicsLineItem *lineItem = addLine(node1->pos().x(), node1->pos().y(), node2->pos().x(), node2->pos().y());
    QPen newPen(Qt::PenStyle::SolidLine);
    newPen.setColor(Qt::blue);
    newPen.setWidth(1);
    lineItem->setPen(newPen);
    addItem(lineItem);
}

void UIModel::drawPath(const std::vector<Node *> &path, Qt::GlobalColor color) {
    QPen newPen(Qt::PenStyle::SolidLine);
    newPen.setColor(color);
    newPen.setWidth(2);

    for(unsigned int i = 1; i < path.size(); ++i) {
        Node *node1 = path[i-1], *node2 = path[i];
        QGraphicsLineItem *lineItem = addLine(node1->pos().x(), node1->pos().y(), node2->pos().x(), node2->pos().y());
        lineItem->setPen(newPen);
        addItem(lineItem);
    }
}

void UIModel::updateGraph() {
    resetScene();
    unsigned int approximateNumOfNodes = (unsigned int) (((float)rand()) / RAND_MAX * 180 + 20); // 20 -- 200 nodes
    m_graph.generateGraph(approximateNumOfNodes, m_sceneSize);
}

void UIModel::resetScene() {
    clear(); // clear items in graphics scene
    m_graph.clearGraph();
    m_startPos = nullptr; // reset these pointers
    m_startNode = nullptr;
    m_goalPos = nullptr;
    m_goalNode = nullptr;

}

void UIModel::slot_newGraph() {
    try {
//        srand(2000);
        updateGraph();
        drawGraphDFS();
    } catch(const std::exception &exp) {
        // resetscene
        resetScene();
        LogUtil::error("Exceptions thrown while generating new graph" + std::string(exp.what()));
    }
}

void UIModel::slot_reset() {
    resetScene();
}

void UIModel::slot_search() {
    if (m_startNode == nullptr || m_goalNode == nullptr) return;
    try {
        std::vector<Node *> path;
        std::string algorithmType =  m_algorithmType->currentText().toStdString();
        if (algorithmType == "Dijkstra") {
            m_graph.searchPathByDijkstra(m_startNode, m_goalNode, path);
            drawPath(path, Qt::red);
        } else if(algorithmType == "A*") {
//            m_graph.searchPathByAstar(m_startNode, m_goalNode, path);
            m_graph.searchPathByAstarAnotherImplementation(m_startNode, m_goalNode, path);
            drawPath(path, Qt::green);
        } else {
            throw std::invalid_argument("Unknown algorithm type");
        }
    } catch(const std::exception &exp) {
        // resetscene
        resetScene();
        LogUtil::error("Exceptions thrown while searching path" + std::string(exp.what()));
    }

}

void UIModel::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {

    QMenu menu(event->widget());
    QString setAsStartPosAction("Set as starting position");
    QString setAsGoalPosAction("Set as goal position");
    menu.addAction(setAsStartPosAction);
    menu.addAction(setAsGoalPosAction);
    QAction *actionChosen = menu.exec(event->screenPos());// show menu in the screen pos and returns the action clicked

    if (actionChosen != nullptr) {
        Node nodeAtCursor(ScenePos(event->scenePos().x(), event->scenePos().y()));
        Node *nearestNode = m_graph.nearestNode(&nodeAtCursor);
        if (nearestNode != nullptr) {
            QGraphicsEllipseItem *item  = addEllipse(nearestNode->pos().x(), nearestNode->pos().y(), 15, 15);
            if (actionChosen->text() == setAsStartPosAction) {
                removeItem(m_startPos);
                m_startPos = item;
                m_startNode = nearestNode;
                item->setBrush(Qt::yellow);
            } else if(actionChosen->text() == setAsGoalPosAction) {
                removeItem(m_goalPos);
                m_goalPos = item;
                m_goalNode = nearestNode;
                item->setBrush(Qt::red);
            }

            addItem(item);
        }

    }
}
