#ifndef UIMODEL_H
#define UIMODEL_H
#include <QGraphicsScene>
#include "Common/SceneSize.h"
#include "Core/Graph.h"
#include <unordered_set>

class QComboBox;

class UIModel : public QGraphicsScene {
    Q_OBJECT
  public slots:
    void slot_newGraph();
    void slot_reset();
    void slot_search();
  public:
    UIModel(QObject *parent, QComboBox *algorithmType);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

  private:
    QComboBox *m_algorithmType;
    SceneSize m_sceneSize;
    Graph m_graph;
    QGraphicsEllipseItem *m_startPos;
    Node *m_startNode;
    QGraphicsEllipseItem *m_goalPos;
    Node *m_goalNode;

    void initUIModel();

    /**
     * @brief drawGraph: draw graph by brute force, used to make sure DFS is drawing correctly
     */
    void drawGraph();
    void drawGraphDFS();
    void drawGraphDFSHelper(Node *currNode, std::unordered_set<Node *> &visited);
    void drawNode(const Node *ptr);
    void drawLine(const Node *node1, const Node *node2);
    void drawPath(const std::vector<Node *> &path, Qt::GlobalColor color);
    void updateGraph();

    void resetScene();
};

#endif // UIMODEL_H
