#include "ConfigService.h"
#include "LogUtil.h"
#include "Packages/pugixml/src/pugixml.hpp"
#include <Exceptions/FileLoadErrorException.h>
#include <sstream>

std::unique_ptr<ConfigService> ConfigService::ms_instance = nullptr;

ConfigService *ConfigService::instance() {
    if (ms_instance == nullptr) {
        ms_instance = std::unique_ptr<ConfigService>(new ConfigService());
    }

    return ms_instance.get();
}

float ConfigService::initializeFromConfig(const std::string &sectionName, const std::string &configName, float defaultValue) const {
    try {
        std::string value = getElementValue(sectionName, configName);
        return std::stof(value);
    } catch (const std::exception &exp) {
        std::stringstream ss;
        ss << "Exceptions thrown while processing Config file: " << sectionName << " : " << configName << ", defaultValue = " << defaultValue << " will be used!" << std::string(exp.what());
        LogUtil::error(ss.str());

        return defaultValue;
    }
}

ConfigService::ConfigService() {

}

std::string ConfigService::getElementValue(const std::string &sectionName, const std::string &configName) const {
    std::string configFilePath = "../Config.xml";
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(configFilePath.c_str());
    if (!result) {
        std::stringstream ss;
        ss << "Error description:" << result.description() << "\n" << "Error offset: " << result.offset;
        throw FileLoadErrorException(ss.str());
    } else {
        pugi::xml_node nodes = doc.child("configuration").child(sectionName.c_str());
        std::string value = nodes.find_child_by_attribute("key", configName.c_str()).attribute("value").value();

        return value;
    }
}

