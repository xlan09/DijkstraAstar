#ifndef SCENESIZE_H
#define SCENESIZE_H
#include "ScenePos.h"

class SceneSize
{
public:
    /**
     * @brief SceneSize
     * @param NWCorner
     * Coordinate
     *  --------------------------->X
     *  |
     *  |
     *  |
     *  |
     *  |
     * \/ Y
     * @param width
     * @param height
     */
    SceneSize();
    SceneSize(ScenePos NWCorner, float width, float height);

    float width() const;
    void setWidth(float width);

    float height() const;
    void setHeight(float height);

    ScenePos NWCorner() const;
    void setNWCorner(const ScenePos &NWCorner);

private:
    ScenePos m_NWCorner;
    float m_width;
    float m_height;
};

#endif // SCENESIZE_H
