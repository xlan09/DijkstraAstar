#include "SceneSize.h"
#include <stdexcept>

SceneSize::SceneSize() : m_NWCorner(ScenePos(0, 0)), m_width(100), m_height(100) {

}

SceneSize::SceneSize(ScenePos NWCorner, float width, float height) : m_NWCorner(NWCorner) {
    if (width <= 0 || height <=0) throw std::invalid_argument("width and height of sceneSize must be larger than 0");
    m_width = width;
    m_height = height;
}

float SceneSize::width() const {
    return m_width;
}

void SceneSize::setWidth(float width) {
    m_width = width;
}

float SceneSize::height() const {
    return m_height;
}

void SceneSize::setHeight(float height) {
    m_height = height;
}
ScenePos SceneSize::NWCorner() const {
    return m_NWCorner;
}

void SceneSize::setNWCorner(const ScenePos &NWCorner) {
    m_NWCorner = NWCorner;
}



