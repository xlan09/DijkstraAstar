#ifndef CONFIGSERVICE_H
#define CONFIGSERVICE_H
#include <memory>

class ConfigService
{
public:
    static ConfigService *instance();
    float initializeFromConfig(const std::string &sectionName, const std::string &configName, float defaultValue) const;

private:
    ConfigService();
    static std::unique_ptr<ConfigService> ms_instance;
    std::string getElementValue(const std::string &sectionName, const std::string &configName) const;
};

#endif // CONFIGSERVICE_H
