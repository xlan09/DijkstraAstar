#ifndef SCENEPOS_H
#define SCENEPOS_H


class ScenePos {
  public:
    ScenePos();
    ScenePos(float x, float y);

    float x() const;
    void setX(float x);

    float y() const;
    void setY(float y);

    float distToOtherPos(const ScenePos &otherPos) const;
    float squaredDistToOtherPos(const ScenePos &otherPos) const;

private:
    float m_x;
    float m_y;
};

#endif // SCENEPOS_H
