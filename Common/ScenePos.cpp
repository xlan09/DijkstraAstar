#include "ScenePos.h"
#include <math.h>

ScenePos::ScenePos() : m_x(0), m_y(0) {

}

ScenePos::ScenePos(float x, float y) : m_x(x), m_y(y) {

}
float ScenePos::x() const {
    return m_x;
}

void ScenePos::setX(float x) {
    m_x = x;
}
float ScenePos::y() const {
    return m_y;
}

void ScenePos::setY(float y) {
    m_y = y;
}

float ScenePos::distToOtherPos(const ScenePos &otherPos) const {
    return sqrt(squaredDistToOtherPos(otherPos));
}

float ScenePos::squaredDistToOtherPos(const ScenePos &otherPos) const {
    return (m_x - otherPos.x()) * (m_x - otherPos.x()) + (m_y - otherPos.y()) * (m_y - otherPos.y());
}





