#ifndef NODE_H
#define NODE_H
#include "Common/ScenePos.h"
#include <vector>

class Node
{
public:
    Node();
    Node(const ScenePos &pos);
    float distToOtherNode(const Node *ptr) const;
    float squaredDistToOtherNode(const Node *ptr) const;

    ScenePos pos() const;

    int Id() const;
    void setId(int id);

    std::vector<Node *> neighbors() const;
    void addNeighbor(Node *ptr);

private:
    int m_id;
    ScenePos m_pos;
    std::vector<Node *> m_neighbors;
};

#endif // NODE_H
