#include "Node.h"

Node::Node() : m_id(0), m_pos(ScenePos(0, 0)) {

}

Node::Node(const ScenePos &pos) : m_id(0), m_pos(pos) {

}

float Node::distToOtherNode(const Node *ptr) const {
    return m_pos.distToOtherPos(ptr->pos());
}

float Node::squaredDistToOtherNode(const Node *ptr) const {
    return m_pos.squaredDistToOtherPos(ptr->pos());
}

ScenePos Node::pos() const {
    return m_pos;
}
int Node::Id() const
{
    return m_id;
}

void Node::setId(int id)
{
    m_id = id;
}
std::vector<Node *> Node::neighbors() const
{
    return m_neighbors;
}

void Node::addNeighbor(Node *ptr)
{
    m_neighbors.push_back(ptr);
}




