#include "Graph.h"
#include "Common/SceneSize.h"
#include <stdlib.h>
#include "Node.h"
#include "Common/ConfigService.h"
#include <math.h>
#include <stdexcept>
#include <limits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include "SearchUtil.h"
#include <stdexcept>

Graph::Graph() {

}

void Graph::generateGraph(unsigned int num, const SceneSize &sceneSize) {
    // clear nodes
    clearGraph();

    float sceneNWCornerX = sceneSize.NWCorner().x();
    float sceneNWCornerY = sceneSize.NWCorner().y();
    float width = sceneSize.width();
    float height = sceneSize.height();

    // calculate number of nodes in each dimension
    float ratio = height / width;
    unsigned int numNodesInXDim = (unsigned int)sqrt(num / ratio);
    unsigned int numNodesInYDim = (unsigned int)(numNodesInXDim * ratio);

    if (numNodesInXDim == 0 || numNodesInYDim == 0) throw std::logic_error("num of nodes in one dimension can not be zero when generating graphs");

    float gridWidth = width / numNodesInXDim;
    float gridHeight = height / numNodesInYDim;
    for(unsigned int i = 0; i < numNodesInYDim; ++i) {
        for(unsigned int j = 0; j < numNodesInXDim; ++j) {
            float randX = sceneNWCornerX + gridWidth * j + gridWidth * (((float)rand()) / RAND_MAX);
            float randY = sceneNWCornerY + gridHeight * i + gridHeight * (((float)rand()) / RAND_MAX);

            std::shared_ptr<Node> temp = std::shared_ptr<Node>(new Node(ScenePos(randX, randY)));
            temp->setId(m_nodes.size() + 1);
            if(j != 0) makeNeighbors(m_nodes[numNodesInXDim * i + j - 1].get(), temp.get());
            if (i != 0) makeNeighbors(m_nodes[numNodesInXDim * (i - 1) + j].get(), temp.get());

            m_nodes.push_back(temp);
        }
    }
}

std::vector<std::shared_ptr<Node> > Graph::nodes() const {
    return m_nodes;
}

unsigned int Graph::getNumNodes() const {
    return m_nodes.size();
}

Node *Graph::getRoot() const {
    if (m_nodes.size() == 0) return nullptr;
    else return m_nodes[0].get();
}

Node *Graph::nearestNode(const Node *ptr) const {
    if (ptr == nullptr) throw std::invalid_argument("Argument can not be null when trying to find nearest node in graph");

    Node *res = nullptr;
    float minDist = std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < m_nodes.size(); ++i) {
        float tempDist = m_nodes[i]->squaredDistToOtherNode(ptr);
        if(tempDist < minDist) {
            minDist = tempDist;
            res = m_nodes[i].get();
        }
    }

    return res;
}

void Graph::clearGraph() {
    m_nodes.clear();
}

bool Graph::searchPathByDijkstra(Node *startNode, Node *endNode, std::vector<Node *> &path) const {
    searchAlgParameterCheck(startNode, endNode);
    path.clear();
    std::unordered_map<Node *, std::pair<Node *, float> > startNodeToAllNodesDist;
    std::unordered_set<Node *> unVisited;

    for(unsigned int i = 0; i < m_nodes.size(); ++i) {
        float currDist = std::numeric_limits<float>::max();
        if (m_nodes[i].get() == startNode) currDist = 0;

        startNodeToAllNodesDist[m_nodes[i].get()] = std::make_pair(nullptr, currDist);
        unVisited.insert(m_nodes[i].get());
    }

    Node *currNode = startNode;
    bool isPathFind = false;
    while(!unVisited.empty()) {
        currNode = SearchUtil::findUnvisitedNodeWithShortestDist(startNodeToAllNodesDist, unVisited);
        if (currNode == nullptr) throw std::logic_error("Node with shortest dist will never be null");
        if (currNode == endNode) {
            isPathFind = true;
            break;
        }

        unVisited.erase(currNode);

        std::vector<Node *> neighbors = currNode->neighbors();
        for(unsigned int  i = 0; i < neighbors.size(); ++i) {
            float tempDist =  startNodeToAllNodesDist[currNode].second + currNode->distToOtherNode(neighbors[i]);
            if (tempDist < startNodeToAllNodesDist[neighbors[i]].second) {
                startNodeToAllNodesDist[neighbors[i]].first = currNode;
                startNodeToAllNodesDist[neighbors[i]].second = tempDist;
            }

        }
    }

    if (isPathFind == false) return false; // no  path find
    // update the path found
    SearchUtil::findPath(startNodeToAllNodesDist, endNode, path);
    return true;
}

bool Graph::searchPathByAstar(Node *startNode, Node *endNode, std::vector<Node *> &path) const {
    searchAlgParameterCheck(startNode, endNode);
    path.clear();

    std::unordered_map<Node *, float> heuriestics;
    generateConsistentHeuristics(heuriestics, endNode);

    std::unordered_set<Node *> visited;
    std::unordered_set<Node *> toBeVisited; // nodes that may be visited
    std::unordered_map<Node *, std::pair<Node *, float>> dist; // dist = dist(startNode, currNode) + distToGo(currNode, endNode)

    dist[startNode] = std::make_pair(nullptr, heuriestics[startNode]);
    toBeVisited.insert(startNode);
    Node *currNode = startNode;
    bool isPathFound = false;
    while(!toBeVisited.empty()) {
        currNode = SearchUtil::findUnvisitedNodeWithShortestDist(dist, toBeVisited);
        if (currNode == nullptr) throw std::logic_error("Node with shortest dist will never be null");
        if (currNode == endNode) {
            isPathFound = true;
            break;
        }

        toBeVisited.erase(currNode);
        visited.insert(currNode);
        std::vector<Node *> neighbors = currNode->neighbors();
        for(unsigned int i = 0; i < neighbors.size(); ++i) {
            if (visited.find(neighbors[i]) == visited.end()) { // if visited, continue
                // if the node has been discovered, then compare its cost, otherwise, just add it to the tobevisited set, no need to compare its cost, since we just discover it, its cost must be infinity.
                float temp = dist[currNode].second - heuriestics[currNode] + currNode->distToOtherNode(neighbors[i]);
                if (toBeVisited.find(neighbors[i]) == toBeVisited.end()) {
                    toBeVisited.insert(neighbors[i]);
                    dist[neighbors[i]] = std::make_pair(currNode, temp + heuriestics[neighbors[i]]);
                } else {
                    if (temp < dist[neighbors[i]].second - heuriestics[neighbors[i]]) {
                        dist[neighbors[i]].first = currNode;
                        dist[neighbors[i]].second = temp + heuriestics[neighbors[i]];
                    }
                }
            }
        }
    }

    if (isPathFound == false) return false;
    SearchUtil::findPath(dist, endNode, path);
    return true;
}

bool Graph::searchPathByAstarAnotherImplementation(Node *startNode, Node *endNode, std::vector<Node *> &path) const {
    searchAlgParameterCheck(startNode, endNode);
    path.clear();

    std::unordered_map<Node *, float> heuriestics;
    generateConsistentHeuristics(heuriestics, endNode);

    std::unordered_map<Node *, std::pair<Node *, float> > dist;
    std::unordered_set<Node *> unVisited;

    for(unsigned int i = 0; i < m_nodes.size(); ++i) {
        float currDist = std::numeric_limits<float>::max();
        if (m_nodes[i].get() == startNode) currDist = 0 + heuriestics[startNode];

        dist[m_nodes[i].get()] = std::make_pair(nullptr, currDist);
        unVisited.insert(m_nodes[i].get());
    }

    Node *currNode = startNode;
    bool isPathFind = false;
    while(!unVisited.empty()) {
        currNode = SearchUtil::findUnvisitedNodeWithShortestDist(dist, unVisited);
        if (currNode == nullptr) throw std::logic_error("Node with shortest dist will never be null");
        if (currNode == endNode) {
            isPathFind = true;
            break;
        }

        unVisited.erase(currNode);
        std::vector<Node *> neighbors = currNode->neighbors();
        for(unsigned int  i = 0; i < neighbors.size(); ++i) {
            if (unVisited.find(neighbors[i]) != unVisited.end()) { // if visited, continue, this is the key difference between A* and Dijkstra, since A* has heuristics and is goal oriented, we can guarantee that each node is at most visited once without overlooking the optimal path. Dijkstra's goal is to search all nodes.
                float temp = dist[currNode].second - heuriestics[currNode] + currNode->distToOtherNode(neighbors[i]);

                if (temp < dist[neighbors[i]].second - heuriestics[neighbors[i]]) {
                    dist[neighbors[i]].first = currNode;
                    dist[neighbors[i]].second = temp + heuriestics[neighbors[i]];
                }
            }

        }
    }

    if (isPathFind == false) return false; // no  path find
    // update the path found
    SearchUtil::findPath(dist, endNode, path);
    return true;

}

void Graph::makeNeighbors(Node *node1, Node *node2) const {
    node1->addNeighbor(node2);
    node2->addNeighbor(node1);
}

void Graph::searchAlgParameterCheck(const Node *startNode, const Node *endNode) const {
    if (startNode == nullptr || endNode == nullptr) throw std::invalid_argument("StartNode and endNode can not be null when searching a path between them");
}

void Graph::generateConsistentHeuristics(std::unordered_map<Node *, float> &heuristics, const Node *endNode) const {
    heuristics.clear();
    for(unsigned int i = 0; i < m_nodes.size(); ++i) {
        heuristics[m_nodes[i].get()] = m_nodes[i]->distToOtherNode(endNode);
    }
}
