#ifndef SEARCHUTIL_H
#define SEARCHUTIL_H
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
class Node;

class SearchUtil
{
public:
    SearchUtil();
    static Node *findUnvisitedNodeWithShortestDist(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, const std::unordered_set<Node *> &unVisited);

    static void findPath(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, Node *endNode, std::vector<Node *> &path);
};

#endif // SEARCHUTIL_H
