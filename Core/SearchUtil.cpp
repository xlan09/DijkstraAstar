#include "SearchUtil.h"
#include <limits>

SearchUtil::SearchUtil() {

}

Node *SearchUtil::findUnvisitedNodeWithShortestDist(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, const std::unordered_set<Node *> &unVisited) {
    Node *nodeWithMinDist = nullptr;
    float minDist = std::numeric_limits<float>::max();

    for(std::unordered_set<Node *>::const_iterator iter = unVisited.begin(); iter != unVisited.end(); ++iter) {
        if (startNodeToAllNodesDist.at(*iter).second < minDist) {
            minDist = startNodeToAllNodesDist.at(*iter).second;
            nodeWithMinDist = *iter;
        }
    }

    return nodeWithMinDist;
}

void SearchUtil::findPath(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, Node *endNode, std::vector<Node *> &path) {
    Node *currNode = endNode;
    while (currNode != nullptr) {
        path.push_back(currNode);
        currNode = startNodeToAllNodesDist.at(currNode).first;
    }
}

