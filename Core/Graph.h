#ifndef GRAPH_H
#define GRAPH_H
#include <memory>
#include <vector>
#include <unordered_map>
class Node;
class SceneSize;

class Graph
{
public:
    Graph();
    void generateGraph(unsigned int num, const SceneSize &sceneSize);

    std::vector<std::shared_ptr<Node> > nodes() const;
    unsigned int getNumNodes() const;
    Node *getRoot() const;
    /**
     * @brief nearestNode: find nearest node inside the graph to the given node
     * Here, brute force is used, for Kd tree implementation, see my RRC project
     * @param ptr
     * @return
     */
    Node *nearestNode(const Node *ptr) const;
    void clearGraph();

    bool searchPathByDijkstra(Node *startNode, Node *endNode, std::vector<Node *> &path) const;
    bool searchPathByAstar(Node *startNode, Node *endNode, std::vector<Node *> &path) const;
    bool searchPathByAstarAnotherImplementation(Node *startNode, Node *endNode, std::vector<Node *> &path) const;

private:
    std::vector<std::shared_ptr<Node> > m_nodes;

    void makeNeighbors(Node *node1, Node *node2) const;
    void searchAlgParameterCheck(const Node *startNode, const Node *endNode) const;
    void generateConsistentHeuristics(std::unordered_map<Node *, float> &heuristics, const Node *endNode) const;
};

#endif // GRAPH_H
